﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Unit_Test_Assignment
{
    public class Unit_Test
    {
        public static void Main()
        {
            Unit_Test divisors = new Unit_Test();
            divisors.numberOfDivisors(100);
        }
        public int numberOfDivisors(int input_number)
        {
            int count = 0;
            if (input_number > 0 && input_number < Math.Pow(10, 7))
            {
                int present_number, next_number;
                for (int index = 1; index < input_number; index++)
                {
                    present_number = index;
                    next_number = present_number + 1;

                    List<int> present_divisor = new List<int>();
                    List<int> next_divisor = new List<int>();

                    for (index = 1; index <= present_number; index++)
                    {
                        if (present_number % index == 0)
                        {
                            present_divisor.Add(index);
                        }
                    }

                    for (index = 1; index <= next_number; index++)
                    {
                        if (next_number % index == 0)
                        {
                            next_divisor.Add(index);
                        }
                    }

                    if (present_divisor.Count == next_divisor.Count)
                    {
                        count++;
                    }

                }
            }
            return count;
        }
    }
}