using NUnit.Framework;
using Unit_Test_Assignment;

namespace assignment_unit_testing.Tests
{
    public class Tests
    {
        [TestCase(15)]
        public void numberOfDivisors_should_return_3(int number)
        {
            Unit_Test divisors = new Unit_Test();
            int result_divisors = divisors.numberOfDivisors(number);
            Assert.AreEqual(3, result_divisors);
        }

        [TestCase(100)]
        public void numberOfDivisors_should_return_15(int number)
        {
            Unit_Test divisors = new Unit_Test();
            int result_divisors = divisors.numberOfDivisors(number);
            Assert.AreEqual(15, result_divisors);
        }
        [TestCase(-1)]
        public void numberOfDivisors_returns_zero(int number)
        {
            Unit_Test divisors = new Unit_Test();
            int result_divisors = divisors.numberOfDivisors(number);
            Assert.Zero(result_divisors);
        }
    }
}