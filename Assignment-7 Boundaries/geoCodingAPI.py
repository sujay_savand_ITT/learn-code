import requests
import json
from requests.exceptions import HTTPError


def get_place_name(message_to_user):
    user_input = input(message_to_user)
    if user_input.isalpha():
        return user_input
    else:
        raise Exception("place name should contain only letters")


def create_url_for_place(place_name):
    http_str = "https://"
    link = "api.opencagedata.com/geocode/v1/json?q="
    api_key = "&key=8b583c90036e42f2980882be58c5c2a3"
    url = http_str +link+place_name+api_key
    return url


def get_response(url):
    try:
        response = requests.get(url)
        return response
    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
    except Exception as err:
        print("couldn't able to reach the request url")

def get_place_coordinates(response):
    response_str = str(response.text)
    response = json.loads(response_str)
    if response['results']:
        print("Latitue: %2.4f" % (response['results'][0]['geometry']['lat']))
        print("Latitue: %2.4f" % (response['results'][0]['geometry']['lng']))
    else:
        print("place not found.")


def main_program():
    place_name = get_place_name("Enter the place name: ")
    url = create_url_for_place(place_name)
    response = get_response(url)
    get_place_coordinates(response)


main_program()
