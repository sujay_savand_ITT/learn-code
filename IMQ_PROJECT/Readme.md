# Why Messaging Queue exists?
Message queues allow different parts of a system to communicate and process operations asynchronously. A message queue provides a lightweight buffer which temporarily stores messages, and endpoints that allow software components to connect to the queue in order to send and receive messages.

# What this system is all about?
The IMQ system is all about the publisher sending messages and subscriber receiving messages asynchronously, where publisher publish message and subscriber receives the message from the queue.

# How a system function?
Various modules, components form a system. Each modules or components work together to make a system work.

# Which problem(s) do you see that you’ll be going to address?
In pub/sub model, To broadcast a message, a component called a publisher simply pushes a message to the topic. Wherein messaging queue system, we can send, store, and receive messages between software components at any volume, without losing messages or requiring other services to be available.

# What are the different use cases where you can see this system can use as a solution?
In ticket booking system we can use messaging queue to book ticket and in online news platform where one can subscribe to many news topics.

# What are the different solutions that are available in the market?
Amazon SQS, Apache kafka, RabbitMQ.