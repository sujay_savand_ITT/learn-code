import sys
sys.path.append('../')
from imq_server.Socket.socket_handler import SocketHandler
from common_folder import constants


class Publisher():

    def __init__(self, socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("\nHello Publisher!")
        name = input("\nEnter your name\n")
        self.socket_handler.send_response(name, constants.PUBLISHER_ROLE)
        response = self.socket_handler.recv_response()
        choice = input(response['data'])
        if(choice == "1"):
            self.create_topic()
        else:
            self.push_message()

    def create_topic(self):
        topic_name = input("\nPlease enter topic name\n")
        self.socket_handler.send_response(
            topic_name, constants.TOPIC_CREATE_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])

    def push_message(self):
        self.socket_handler.send_response(None, constants.PUSH_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])
        data = input("\nPlease enter topic name and push message\n")
        self.socket_handler.send_response(data, constants.PUSH_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])
