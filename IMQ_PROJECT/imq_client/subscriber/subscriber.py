import sys
sys.path.append('../')
from common_folder import constants
from imq_server.Socket.socket_handler import SocketHandler


class Subscriber:

    def __init__(self, socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("\nHello Subscriber!")
        name = input("Enter your name\n")
        self.socket_handler.send_response(name, constants.SUBSCRIBER_ROLE)
        response = self.socket_handler.recv_response()

        choice = input(response['data'])
        if(choice == "1"):
            self.subscribe_to_topic()
        else:
            self.pull_message()

    def subscribe_to_topic(self):
        self.socket_handler.send_response(None, constants.SUBSCRIBE_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])
        topic_name = input("\nPlease enter topic name\n")
        self.socket_handler.send_response(
            topic_name, constants.SUBSCRIBE_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])

    def pull_message(self):
        self.socket_handler.send_response(None, constants.PULL_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])
        topic_name = input("\nPlease enter topic name to pull messages\n")
        self.socket_handler.send_response(topic_name, constants.PULL_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data'])
