import sys
sys.path.append('../')
from imq_server.database.database_handler import DataBaseHandler
from common_folder import constants

database_obj = DataBaseHandler()


class ClientsRequest():
    def __init__(self, socket_handler, role, client_name):
        self.socket_handler = socket_handler
        self.client_name = client_name
        self.handle_request(client_name, role)

    def handle_request(self, name, role):
        if(role):
            if(int(role) == 1):
                response_to_send = 'Please select one of the option 1.Create Topic 2.Push Message\n'
            else:
                response_to_send = 'Please select one of the option 1.Subscribe to Topic 2.Pull Messages\n'
            if not database_obj.get_user_id(name):
                database_obj.insert_into_clients_table(name, role)
            else:
                database_obj.get_user_id(name)
            self.socket_handler.send_response(
                response_to_send, constants.NO_REQUEST)
            response = self.socket_handler.recv_response()
            if(response['request_type'] == constants.TOPIC_CREATE_REQUEST):
                self.handle_topic_creation(response['data'])
            if(response['request_type'] == constants.PUSH_REQUEST):
                self.push_message()
            if(response['request_type'] == constants.SUBSCRIBE_REQUEST):
                self.add_subscription(self.client_name)
            if(response['request_type'] == constants.PULL_REQUEST):
                self.handle_pull_request()
        else:
            response_to_send = 3
            self.socket_handler.send_response(
                response_to_send, constants.NO_REQUEST)
        self.socket_handler.close_socket()
        exit()

    def push_message(self):
        self.socket_handler.send_response(
            self.get_topics_names(), constants.PUSH_REQUEST)
        message = self.socket_handler.recv_response()['data']
        data = message.split(' ', 2)
        topic_name = data[0]
        message = data[1]
        topic_id = str(database_obj.get_topic_id(topic_name))
        publisher_id = str(database_obj.get_user_id(self.client_name))
        message_id = database_obj.save_messages(
            topic_id, message, publisher_id)
        subscribers = database_obj.get_subcribers(topic_id)
        if(subscribers):
            for subscriber in subscribers:
                subscriber_id = subscriber[0]
                database_obj.insert_into_topic_queue(
                    topic_name, subscriber_id, message_id)
        response_to_send = 'Pushed message successfully'
        self.socket_handler.send_response(
            response_to_send, constants.PUSH_REQUEST)

    def handle_topic_creation(self, topic_name):
        if not database_obj.get_topic_id(topic_name):
            database_obj.insert_into_topics_table(topic_name, self.client_name)
            database_obj.create_queue_for_topic(topic_name)
            print(topic_name)
            response_to_send = 'Topic created successfully'
            self.socket_handler.send_response(
                response_to_send, constants.TOPIC_CREATE_REQUEST)
            print(response_to_send)
        else:
            response_to_send = 'Topic exists with that name'
            self.socket_handler.send_response(
                response_to_send, constants.TOPIC_CREATE_REQUEST)
        print(response_to_send)

    def add_subscription(self, client_name):
        self.socket_handler.send_response(
            self.get_topics_names(), constants.PUSH_REQUEST)
        topic_name = self.socket_handler.recv_response()['data']
        topic_id = database_obj.get_topic_id(topic_name)
        subscriber_id = database_obj.get_user_id(client_name)
        database_obj.add_subscription(str(topic_id), str(subscriber_id))
        response_to_send = 'Subscribed successfully'
        self.socket_handler.send_response(
            response_to_send, constants.PUSH_REQUEST)

    def get_messages(self, topic_name):
        subscriber_id = str(database_obj.get_user_id(self.client_name))
        messages = database_obj.get_messages(topic_name, subscriber_id)
        database_obj.update_message_status(topic_name, subscriber_id)
        messages_to_send = ''
        if(messages):
            for message in messages:
                messages_to_send = messages_to_send + message[0]+'\n'
            return messages_to_send
        return False

    def get_topics_names(self):
        topics = database_obj.get_topics()
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1]+'\n'
            return messages_to_send
        return False

    def get_subscribed_topics(self, subscriber_name):
        subscriber_id = database_obj.get_user_id(subscriber_name)
        topics = database_obj.get_subscribed_topics(subscriber_id)
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1]+'\n'
            return messages_to_send
        return False

    def handle_pull_request(self):
        self.socket_handler.send_response(self.get_subscribed_topics(
            self.client_name), constants.PUSH_REQUEST)
        topic_name = self.socket_handler.recv_response()['data']
        response_to_send = self.get_messages(topic_name)
        if not (response_to_send):
            response_to_send = 'Sorry! No Messages to pull!'
        self.socket_handler.send_response(
            response_to_send, constants.PULL_REQUEST)
