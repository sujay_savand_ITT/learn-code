import sys
sys.path.append('../')
from imq_client.subscriber.subscriber import Subscriber
from imq_client.publisher.publisher import Publisher
from common_folder import constants
from imq_server.Socket.socket_handler import SocketHandler

class SocketClient(SocketHandler):

    def __init__(self):
        super().__init__()

    def read_choice(self):
        request = input(
            "Please select one of the following choices \n1.Publisher \n2.Subscriber\n")
        if(int(request) == 1):
            Publisher(self.socket_obj)
        elif(int(request) == 2):
            Subscriber(self.socket_obj)
        else:
            print('invalid credentials')

    def read_response(self):
        response = self.recv_response()
        print('Response from Server: '+response['data'])
        self.read_choice()
        self.close_socket()


if __name__ == '__main__':
    client = SocketClient()
    client.create_socket()
    client.connect_socket()
    client.read_response()
