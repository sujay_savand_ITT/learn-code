import datetime
import sys
sys.path.append('../')
from common_folder.enum import DateTime
from common_folder.tools.utility import Utility
from collections import deque


utility = Utility


class DeadLetterQueue():
    queue = deque()
    dead_letter_queue = []

    def enqueue(self, message):
        self.queue.append(message)
        self.dequeue(message)

    def dead_letter_queue(self, message):
        self.dead_letter_queue = self.queue.append(message)
        return self.queue.popleft(message)

    def dequeue(self, message):
        current_time = DateTime.Current_Time
        expiry_time = DateTime.NextDay_Time
        message = {
            'message': message,
            'expires_at': expiry_time
        }
        current_time = datetime.datetime.strptime(current_time, '%Y-%m-%d %H:%M:%S')
        expiry_time = datetime.datetime.strptime(expiry_time, '%Y-%m-%d %H:%M:%S')
        if(expiry_time > current_time):
            self.dead_letter_queue(message)
        return self.queue
