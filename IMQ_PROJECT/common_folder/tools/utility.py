import datetime, time


class Utility():

    def time_stamp(self):
        timestamp = time.time()
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d::%H:%M:%S - ')
