import datetime
from enum import Enum


class Response(Enum):
    OK = 200
    CREATED = 201
    BAD_REQUEST = 400


class DateTime(Enum):
    Current_Time = datetime.datetime.today()
    Previous_Time = datetime.datetime.today() - datetime.timedelta(days=1)
    NextDay_Time = datetime.datetime.today() + datetime.timedelta(days=1)
