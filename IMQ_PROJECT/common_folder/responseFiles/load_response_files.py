import os.path
import datetime
import time
import sys
sys.path.append('../')
from common_folder.enum import Response
from imq_exception_handler.io_exception import IOErrorException


class ResponseFiles():

    def create_output_directory(self, output_directory_name):
        if not os.path.exists(output_directory_name):
            os.makedirs(output_directory_name)
        return output_directory_name

    def write_response_data_to_file(self, output_directory_name, address, connection_time, response):
        try:
            output_file = output_directory_name + '/' + \
                address[0] + '_' + str(address[1]) + '.txt'
            if os.path.exists(output_file):
                with open(output_file, "a") as file:
                    file.write(connection_time + response + '\n')
            else:
                with open(output_file, "w") as file:
                    file.write(connection_time + response + '\n')

        except IOErrorException:
            print("IO exceptions")

        return Response.CREATED.value
