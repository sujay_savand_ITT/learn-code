import sys
sys.path.append('../')
import _thread
from imq_server.Socket.socket_handler import SocketHandler
from common_folder.responseFiles.load_response_files import ResponseFiles
from imq_server.threaded_client_commn import ClientThreadHandler


class SocketServer(SocketHandler):
    thread_count = 0

    def __init__(self):
        super().__init__()

    def accept_connection(self):
        while True:
            self.client, self.address = self.socket_accept()
            print('Connected to: ' +
                  self.address[0] + ':' + str(self.address[1]))
            self.create_thread()
        self.close_socket()

    def create_thread(self):
        _thread.start_new_thread(self.threaded_client,
                                 (self.client, self.address))
        self.thread_count += 1
        print('Client Number: ' + str(self.thread_count))

    def threaded_client(self, connection, address):
        ClientThreadHandler(connection, address)


if __name__ == '__main__':
    try:
        server = SocketServer()
        server.create_socket()
        server.socket_bind()
        server.socket_listen()
        server.accept_connection()
    except Exception as ex:
        print("something went wrong, closing connections")
        print(ex)
