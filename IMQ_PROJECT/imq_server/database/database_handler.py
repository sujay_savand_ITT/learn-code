import sys
sys.path.append('../')
from imq_server.database.init import Initializer
import mysql.connector



class DataBaseHandler(Initializer):

    def __init__(self):
        super(DataBaseHandler, self).__init__()

    def insert_into_clients_table(self, client_name, type):
        query = "INSERT into `clients` (`client_name`,`type`) values('" + \
            client_name+"','"+str(type)+"')"
        self.execute_query(query)

    def insert_into_topic_table(self, topic_name, message, client_name):
        publisher_id = str(self.get_user_id(client_name))
        query = "INSERT into `"+topic_name + \
            "`(`received_time`,`data`,`created_by`) values( NOW(),'" + \
            message+"',"+str(publisher_id)+")"
        self.execute_query(query)

    def get_user_id(self, client_name):
        query = "SELECT id from clients WHERE client_name = '"+client_name+"'"
        return self.fetchone_query(query)

    def insert_into_topics_table(self, topic_name, client_name):
        publisher_id = str(self.get_user_id(client_name))
        query = "INSERT into `topics` (`topic_name`,`created_by`) values('" + \
            topic_name+"','"+str(publisher_id)+"')"
        self.execute_query(query)

    def get_topic_id(self, topic_name):
        query = "SELECT id from topics WHERE topic_name = '"+topic_name+"'"
        return self.fetchone_query(query)

    def get_message_id(self, message):
        query = "SELECT id from messages WHERE message like '"+message+"'"
        return self.fetchone_query(query)

    def add_subscription(self, topic_id, client_id):
        query = "INSERT into `subscribers` (`topic_id`,`client_id`) values('"+str(
            topic_id)+"','"+str(client_id)+"')"
        self.execute_query(query)

    def get_subcribers(self, topic_id):
        query = 'SELECT client_id from subscribers WHERE topic_id = ' + \
            str(topic_id)
        return self.fetch_query(query)

    def save_messages(self, topic_id, message, publisher_id):
        query = "INSERT into `messages` (`message`,`created_by`,`topic_id`) values('" + \
            message+"','"+str(publisher_id)+"','"+str(topic_id)+"')"
        self.execute_query(query)
        return self.get_message_id(message)

    def get_messages(self, topic_name, subscriber_id):
        query = 'SELECT `message` FROM `messages` WHERE `id` IN (SELECT `message_id` from `'+str(topic_name)+'_queue` WHERE subscriber_id = '+str(
            subscriber_id)+' AND message_status_id = (SELECT id from message_status WHERE status = "READY"))'
        return self.fetch_query(query)

    def update_message_status(self, topic_name, subscriber_id):
        query = 'UPDATE `'+str(topic_name)+'_queue` SET message_status_id = (SELECT id from message_status WHERE status = "DELIVERED") WHERE subscriber_id = ' + \
            str(subscriber_id) + \
            ' AND message_status_id = (SELECT id from message_status WHERE status = "READY")'
        return self.execute_query(query)

    def create_queue_for_topic(self, topic_name):
        query = 'CREATE TABLE IF NOT EXISTS `'+topic_name+'_queue` (`id` INT AUTO_INCREMENT PRIMARY KEY, `message_id` INT,`subscriber_id` INT NOT NULL,message_status_id INT NOT NULL DEFAULT 1,CONSTRAINT '+topic_name + \
            '_queue_fk_clients FOREIGN KEY (subscriber_id) REFERENCES clients(id), CONSTRAINT '+topic_name + \
            '_queue_fk_status FOREIGN KEY (message_status_id) REFERENCES message_status(id),CONSTRAINT ' + \
            topic_name + \
                '_queue_fk_message FOREIGN KEY (message_id) REFERENCES messages(id))'
        self.execute_query(query)

    def insert_into_topic_queue(self, topic_name, subscriber_id, message_id):
        query = "INSERT into `"+topic_name + \
            "_queue` (`message_id`,`subscriber_id`) values('" + \
            str(message_id)+"','"+str(subscriber_id)+"')"
        self.execute_query(query)

    def get_topics(self):
        query = "SELECT id,topic_name FROM `topics`"
        return self.fetch_query(query)

    def get_subscribed_topics(self, subscriber_id):
        query = "SELECT id,topic_name from `topics` WHERE id IN(SELECT topic_id from subscribers WHERE `client_id` = '"+str(
            subscriber_id)+"')"
        return self.fetch_query(query)
