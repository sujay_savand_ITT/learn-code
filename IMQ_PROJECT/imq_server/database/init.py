from imq_exception_handler.db_connector_exception import DBConnectionException
import mysql.connector


class Initializer:
    database_name = 'IMQ'

    def __init__(self):
        self.connect()
        self.create_database()
        self.change_database(self.database_name)
        self.create_clients_table()
        self.create_topics_table()
        self.create_subscriber_table()
        self.create_messages_table()
        self.create_message_status_table()
        self.insert_message_status()

    def connect(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="sujay", autocommit=False
        )

    def create_database(self):
        self.mycursor = self.mydb.cursor()
        query = "CREATE DATABASE IF NOT EXISTS "+self.database_name
        self.execute_query(query)

    def change_database(self, database_name):
        query = 'USE '+database_name
        self.execute_query(query)

    def create_clients_table(self):
        query = "CREATE TABLE IF NOT EXISTS clients (id INT AUTO_INCREMENT PRIMARY KEY, client_name VARCHAR(255) NOT NULL UNIQUE, type ENUM('1','2'))"
        self.execute_query(query)

    def create_topics_table(self):
        query = 'CREATE TABLE IF NOT EXISTS topics (id INT AUTO_INCREMENT PRIMARY KEY, topic_name VARCHAR(255) NOT NULL UNIQUE,created_by INT, CONSTRAINT topics_fk_clients FOREIGN KEY (created_by) REFERENCES clients(id))'
        self.execute_query(query)

    def create_subscriber_table(self):
        query = "CREATE TABLE IF NOT EXISTS subscribers (id INT AUTO_INCREMENT PRIMARY KEY, client_id  INT NOT NULL, topic_id INT NOT NULL, CONSTRAINT fk_topics FOREIGN KEY (topic_id) REFERENCES topics(id),CONSTRAINT subscribers_fk_clients FOREIGN KEY (client_id) REFERENCES clients(id))"
        self.execute_query(query)

    def create_messages_table(self):
        query = 'CREATE TABLE IF NOT EXISTS `messages` (`id` INT AUTO_INCREMENT PRIMARY KEY, `created_at` datetime DEFAULT NOW(), message VARCHAR(255),created_by INT NOT NULL,topic_id INT NOT NULL,expires_at datetime, CONSTRAINT messages_fk_topics FOREIGN KEY (topic_id) REFERENCES topics(id),CONSTRAINT messages_fk_clients FOREIGN KEY (created_by) REFERENCES clients(id))'
        self.execute_query(query)

    def create_message_status_table(self):
        query = 'CREATE TABLE IF NOT EXISTS `message_status`(`id` INT AUTO_INCREMENT PRIMARY KEY,`status` VARCHAR(255) NOT NULL UNIQUE)'
        self.execute_query(query)

    def insert_message_status(self):
        query = 'INSERT IGNORE INTO `message_status`(`status`) VALUES("READY"),("DELIVERED"),("DEAD")'
        self.execute_query(query)

    def execute_query(self, query):
        try:
            if(self.mydb.in_transaction):
                self.mydb.rollback()
            self.mydb.start_transaction()
            self.mycursor.execute(query)
            self.mydb.commit()
        except DBConnectionException:
            self.mydb.rollback()
            return False

    def fetchone_query(self, query):
        try:
            self.mycursor.execute(query)
            result = self.mycursor.fetchone()
            if result:
                return result[0]
            return False
        except DBConnectionException:
            return False

    def fetch_query(self, query):
        try:
            self.mycursor.execute(query)
            result = self.mycursor.fetchmany(100)
            if result:
                return result
            return False
        except DBConnectionException:
            return False
