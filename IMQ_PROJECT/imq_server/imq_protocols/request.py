import sys
sys.path.append('../')
from imq_server.imq_protocols.imq_protocol import ImqProtocol
from dataclasses import dataclass


@dataclass
class Request(ImqProtocol):
    requestType: str = "connect_to_topic"
