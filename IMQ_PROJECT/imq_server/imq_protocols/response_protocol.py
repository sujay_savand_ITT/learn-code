from imq_server.imq_protocols.imq_protocol import ImqProtocol
from dataclasses import dataclass


@dataclass
class Response(ImqProtocol):
    messageStatus: str = "Sent!"
