from dataclasses import dataclass


@dataclass
class ImqProtocol:
    data: str
    request_type: int
    format: str = "json"
