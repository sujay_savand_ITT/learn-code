
class Cryptography():
    def encrypt(self, data, position):
        result = ""
        for i in range(len(data)):
            char = data[i]

            if (char.isupper()):
                result += chr((ord(char) + position - 65) % 26 + 65)
            else:
                result += chr((ord(char) + position - 97) % 26 + 97)

        return result
