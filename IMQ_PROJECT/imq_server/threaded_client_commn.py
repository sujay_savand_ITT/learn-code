import sys
sys.path.append('../')
from imq_client.clientsHandler.clients_service import ClientsRequest
from imq_server.database.database_handler import DataBaseHandler
from imq_server.imq_protocols.serialize_and_deserialize_json import SerializeAndDeserializeJSON
from imq_server.imq_protocols.request import Request
from common_folder import constants
from imq_server.Socket.socket_handler import SocketHandler

database_obj = DataBaseHandler()


class ClientThreadHandler():

    def __init__(self, socket, address):
        self.socket_handler = SocketHandler()
        self.socket_handler.set_socket(socket)
        self.handle_connection()

    def handle_connection(self):
        while True:
            response_to_send = 'Welcome to the Server'
            self.socket_handler.send_response(response_to_send, constants.NO_REQUEST)
            data =  self.socket_handler.recv_response()
            if not data:
                break
            if(data['request_type'] == constants.PUBLISHER_ROLE):
                ClientsRequest(self.socket_handler, data['request_type'], data['data'])
            if(data['request_type'] == constants.SUBSCRIBER_ROLE):
                ClientsRequest(self.socket_handler, data['request_type'], data['data'])
        self.socket_handler.close_socket()
        exit()
