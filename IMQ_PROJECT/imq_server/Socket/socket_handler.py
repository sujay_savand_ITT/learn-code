import sys
sys.path.append('../../')
from imq_exception_handler.socket_exception import SocketException
from imq_server.imq_protocols.request import Request
from imq_server.imq_protocols.serialize_and_deserialize_json import SerializeAndDeserializeJSON
import socket


class SocketHandler():

    def __init__(self):
        self.host = socket.gethostname()
        self.port = 8080

    def create_socket(self):
        try:
            self.socket_obj = socket.socket()
            return True
        except SocketException:
            return False

    def set_socket(self, socket):
        self.socket_obj = socket

    def socket_bind(self):
        try:
            self.socket_obj.bind((self.host, self.port))
            return True
        except SocketException:
            self.socket_obj.close()

    def socket_listen(self):
        try:
            print('Waiting for a Connection..')
            self.socket_obj.listen(5)
            return True
        except SocketException:
            self.socket_obj.close()

    def connect_socket(self):
        try:
            print('Waiting for connection...')
            self.socket_obj.connect((self.host, self.port))
            print('Connected..!!')
            return True
        except SocketException:
            self.socket_obj.close()

    def socket_accept(self):
        try:
            client, address = self.socket_obj.accept()
            return client, address
            return True
        except SocketException:
            self.socket_obj.close()

    def close_socket(self):
        try:
            self.socket_obj.close()
            print("Connection closed")
            return True
        except SocketException:
            self.socket_obj.close()

    def recv_response(self):
        try:
            data = self.socket_obj.recv(2048)
            if not data:
                return False
            parsed_response = SerializeAndDeserializeJSON.get_json_decoded(data)
            return parsed_response
        except SocketException:
            self.socket_obj.close()

    def send_response(self, request, type):
        try:
            response_header = Request(request, type)
            parsed_header = SerializeAndDeserializeJSON.get_json_encoded(response_header)
            self.socket_obj.send(str.encode(parsed_header))
            return True
        except SocketException:
            self.socket_obj.close()
