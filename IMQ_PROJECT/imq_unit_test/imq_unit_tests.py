import sys
sys.path.append('../')
from imq_server.imq_protocols.serialize_and_deserialize_json import SerializeAndDeserializeJSON
from imq_server.imq_protocols.cryptography import Cryptography
from common_folder.responseFiles.load_response_files import ResponseFiles
from imq_client.clientsHandler.clients_service import ClientsRequest
from imq_client.client import SocketClient
from imq_server.server import SocketServer
from common_folder.enum import Response
import unittest


class IMQSystemTest(unittest.TestCase):
    client_obj = SocketClient()
    server_obj = SocketServer()
    file_handler_obj = ResponseFiles()
    ceaser_cipher = Cryptography()

    def test_ceaser_cipher_test_position_3(self):
        data = 'Hey'
        position = 3
        expected_encryted_data = 'Khb'

        self.assertEqual(expected_encryted_data,
                         self.ceaser_cipher.encrypt(data, position))

    def test_ceaser_cipher_test_position_10(self):
        data = 'Hello Sujay!'
        position = 10
        expected_encryted_data = 'RovvyxCetkiy'

        self.assertEqual(expected_encryted_data,
                         self.ceaser_cipher.encrypt(data, position))

    def test_get_json_encoded(self):
        string_data = "message"
        encoded_json = '"\\"message\\""'
        self.assertEqual(SerializeAndDeserializeJSON.get_json_encoded(
            string_data), encoded_json)

    def test_json_encoded(self):
        employee = '{"id":"09"}'
        self.assertIsInstance(
            SerializeAndDeserializeJSON.get_json_encoded(employee), str)

    def test_socket_creation(self):
        self.assertTrue((type(self.client_obj.create_socket())))
        self.assertTrue((type(self.server_obj.create_socket())))

    def test_socket_bind(self):
        self.server_obj.create_socket()
        self.assertTrue((type(self.server_obj.socket_bind())))

    def test_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.assertTrue((type(self.server_obj.socket_listen())))

    def test_socket_connection_establishment(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.assertTrue((type(self.client_obj.connect_socket())))

    def test_socket_request_acception(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.client_obj.connect_socket()
        self.assertTrue((type(self.server_obj.socket_accept())))

    def test_get_topics_names(self):
        topic = str("topic_names")
        if ClientsRequest.get_topics_names is not None:
            self.assertTrue(ClientsRequest.get_topics_names(topic))

    def test_get_subscribed_topics_unsubscribed_false(self):
        non_subscribed_topic_names = 'fake_topics'
        self.assertFalse(ClientsRequest.get_subscribed_topics(self, str(
            non_subscribed_topic_names)), 'topics which were not subscribed are shown!')

    def test_write_response_data_to_file(self):
        output_directory = '../output'
        address = ['192.168.1.85', '80777']
        mock_connection_time = '2021-02-21::13:50:47'
        message = 'Hello!'
        response = self.file_handler_obj.write_response_data_to_file(
            output_directory, address, mock_connection_time, message)
        self.assertEquals(response, Response.CREATED.value)
